const express = require('express');
const conf = require('./server.conf');
const request = require('request');
const app = express();
const port = process.env.PORT || 5656;

app.set('view engine', 'pug');
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.redirect('/grocery');
});

app.get('/grocery', (req, res) => {
  request.get(conf.BASE_URL + 'grocery', (err, response, body) => {
    if (err) {
        return next(err);
    }
    res.render('index', JSON.parse(body));
  });
});

app.get('/furniture', (req, res) => {
  request.get(conf.BASE_URL + 'furniture', (err, response, body) => {
    if (err) {
        return next(err);
    }
    res.render('index', JSON.parse(body));
  });
});

app.get('/babies', (req, res) => {
  request.get(conf.BASE_URL + 'babies', (err, response, body) => {
    if (err) {
        return next(err);
    }
    res.render('index', JSON.parse(body));
  });
});

app.get('/cleaning', (req, res) => {
  request.get(conf.BASE_URL + 'cleaning', (err, response, body) => {
    if (err) {
        return next(err);
    }
    res.render('index', JSON.parse(body));
  });
});

app.listen(port, () => {
     console.log(`Server running on: http://localhost:${port}`);
})
